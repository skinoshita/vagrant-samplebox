#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{php php-common php-devel php-mbstring php-xml php-pear php-mysql}.each do |pkg|
  package pkg do
    action :install
  end
end